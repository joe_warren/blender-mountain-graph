import bpy
import json
import math 
import mathutils


def make_fn_grid(samples, fn):
   border = 20
   def vert(x,y):
      fracx = float(x)/samples
      fracy = float(y)/samples
      vec = mathutils.Vector((fracx, fracy, fn(x,y)))
      noise = mathutils.noise.noise_vector(vec*10)* gaussian(2*(fracy-1.0))
      noise[2] = 0.0
      return vec + noise * 0.025
 
   def index(x, y):
      return y + (samples) * x

   def faces(i, j):
       return [[index(i, j), index(i,j+1), index(i+1,j)],
               [index(i,j+1), index(i+1,j+1), index(i+1, j)]]
   verts = [vert(x,y) for x in range(-border,samples+border) for y in range(0,samples)]
   faces = [f for x in range(0,(samples + 2*border)-1) for y in range(0,samples-1) for f in faces(x,y)]
   return(verts, faces)

def normalize(d):
    low = min(d)
    high = max(d)
    r = high - low
    return [ (v-low)/r for v in d]


def gaussian(x):
    return math.exp(-x * x / 2)

def clamp(d, x):
    if x < 0:
        return d[0]
    elif x > len(d)-1:
        return d[-1]
    else:
        return d[x]

def height_fn(d):
    samples = len(d)
    rand_sz = 0.05
    rand_scale = 30
    def fn(x,y):
        fracx = x/samples
        fracy = y/samples
        exp = 2.0+6*math.fabs(mathutils.noise.noise(mathutils.Vector((5*fracx,0,0))))
        falloff = math.pow(exp, -2*fracy) * ( 1.0-gaussian(5.0*(fracy-1.0)) )  

        randomness = rand_sz * mathutils.noise.noise(mathutils.Vector((rand_scale*fracx,rand_scale*fracy,0))) * gaussian(3.0*(fracy-0.5))
        val = clamp(d,x)*0.6 + 0.1
        return val * (falloff+randomness)
    return fn

with open("graph.json") as f:
    d = normalize(json.load(f))
    mesh = bpy.data.meshes.new(name="graph")
    ob = bpy.data.objects.new("graph", mesh)
    scn = bpy.context.scene
    scn.objects.link(ob)
    scn.objects.active = ob
    ob.select = True

    
    (verts, faces) = make_fn_grid(len(d), height_fn(d))

    mesh.from_pydata(verts,[], faces)

    mat = bpy.data.materials.get("Ground")
    if mat is None:
        mat = bpy.data.materials.new(name="Ground")

    if ob.data.materials:
        ob.data.materials[0] = mat
    else:
        ob.data.materials.append(mat)

    bpy.context.scene.render.filepath = 'output.png'
    bpy.ops.render.render(write_still=True)
