import json

with open("data.json") as f:
    d = json.load(f)["d"]
    o = [v[1] for v in d]
    with open("graph.json","w") as of:
        json.dump(o, of)
